#
# Cookbook Name:: AdamPhp
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
yum_package 'httpd24' do
	supports :status => true
    action :install
end

yum_package 'php56' do
	action :install
end

service "httpd" do
	action [:enable, :start]
end

template '/var/www/html/index.php' do
  source 'index.php.erb'
end

directory "/var/adam" do
  owner 'root'
  group 'root'
  mode '0777'
  action :create
end

directory "/var/www/html/schedule-manager" do
  owner 'root'
  group 'root'
  mode '0777'
  action :create
end

template "/var/adam/wrapper.sh" do
  source "wrapper.sh.erb"
  mode '0777'
end

template "/var/adam/id_rsa.pub" do
  source "id_rsa.pub.erb"
  mode '0777'
end

template "/var/adam/id_rsa" do
  source "id_rsa.erb"
  mode '0777'
end

deploy 'sm' do
  repo 'git@bitbucket.org:illinoisstateweb/schedule-manager.git'
  deploy_to '/var/www/html'
  ssh_wrapper '/var/adam/wrapper.sh'
  action :deploy
end
